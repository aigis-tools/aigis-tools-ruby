require_relative '../prince'

describe Prince do
  describe '.get_max_charisma' do
    context 'given level 1' do
      it 'returns 30' do
        expect(Prince.get_max_charisma(1)).to eql(30)
      end
    end
  end

  describe '.get_max_stamina' do
    context 'given level 1' do
      it 'returns 12' do
        expect(Prince.get_max_stamina(1)).to eql(12)
      end
    end
  end

  describe '.lvl_to_exp' do
    context 'given level 100 and 1 exp to next' do
      it 'returns 339620 - 1' do
        expect(Prince.lvl_to_exp(100, 1)).to eql(339620 - 1)
      end
    end
    context 'given level 101' do
      it 'returns 339620' do
        expect(Prince.lvl_to_exp(101)).to eql(339620)
      end
    end
    context 'given level 250 and 1 exp to next' do
      it 'returns 4375265 - 1' do
        expect(Prince.lvl_to_exp(250, 1)).to eql(4375265 - 1)
      end
    end
    context 'given level 251' do
      it 'returns 4375265' do
        expect(Prince.lvl_to_exp(251)).to eql(4375265)
      end
    end

    context 'when used together with .exp_to_lvl' do
      it 'has no round-trip conversion problems' do
        10000.times do
          rand_exp = rand(0..10000000)
          expect(Prince.lvl_to_exp(*Prince.exp_to_lvl(rand_exp))).to eql(rand_exp)
        end
      end
    end
  end

  describe '.gain_exp' do
    context 'given 1112522 experience at level 1' do
      prince = Prince.new(1)
      prince.gain_exp(1112522)
      it 'refills charisma 156 times' do
        expect(prince.charisma_refill_count).to eql(156)
      end
      it 'refills stamina 3 times' do
        expect(prince.stamina_refill_count).to eql(3)
      end
    end
  end
end
