#!/usr/bin/env ruby

# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org>

# Decoding of the encrypted POST responses from the Millenium War Aigis
# network protocol. The original Lua implementation from Lzlis has been
# converted to Ruby. For more details see:
#   http://millenniumwaraigis.wikia.com/wiki/User_blog:Lzlis/Interpreting_POST_responses

require 'json'
require 'base64'

###############################################################################
# Command line options handling                                               #
###############################################################################

convert_to_json = false

if ARGV.include?('-j')
  begin
    require 'active_support/core_ext/hash'
  rescue LoadError
    abort "No 'activesupport' gem found, so the -j option is not supported.\n"
  end
  ARGV.delete('-j')
  convert_to_json = true
end

if ARGV.include?('-x')
  ARGV.delete('-x')
  convert_to_json = false
end

if ARGV.empty?
  puts "Usage: #{$PROGRAM_NAME} [-x|-j] recorded-browser-session.har3\n"
  puts 'Where:'
  puts '   -x : Use the original XML format for output (default).'
  puts '   -j : Use prettified JSON output (needs "activesupport" ruby gem).'
  puts
  puts 'The input file is expected to be in HAR format and can be created'
  puts 'by the Chromium browser if you use Ctrl+Shift+I hotkey to invoke'
  puts 'Developer Tools, then record the browsing session, right click and'
  puts 'pick "Save as HAR with Content" in the drop down menu.'
  exit 1
end

unless File.exist?(ARGV[0])
  abort "Need a file name in the command line ('#{ARGV[0]}' is not a file).\n"
  exit 1
end

###############################################################################
# Simplistic XOR decryption                                                   #
###############################################################################

# Decrypt a data buffer using a 8-bit xor key
def xor_text(text, key)
  text.unpack('C*').map { |x| x ^ key }.pack('C*')
end

# Guess a 8-bit xor key for decryption
def find_xor_key(text, pattern)
  0.upto(0xFF) { |key| return key if xor_text(text, key).include?(pattern) }
end

###############################################################################
# Some variant of LZ77 data decompression                                     #
###############################################################################

def lz_get_outbuf_bytesize(inbuf)
  result = 0
  inbuf.byteslice(0, 5).unpack('C*').each.with_index do |b, i|
    result |= (b & 0x7F) << (i * 7)
    return [result, i + 1] if (b & 0x80).zero?
  end
end

def lz_decompress_buffer(inbuf)
  lookup = "
    0001 0804 1001 2001 0002 0805 1002 2002 0003 0806 1003 2003 0004 0807 1004 2004
    0005 0808 1005 2005 0006 0809 1006 2006 0007 080A 1007 2007 0008 080B 1008 2008
    0009 0904 1009 2009 000A 0905 100A 200A 000B 0906 100B 200B 000C 0907 100C 200C
    000D 0908 100D 200D 000E 0909 100E 200E 000F 090A 100F 200F 0010 090B 1010 2010
    0011 0A04 1011 2011 0012 0A05 1012 2012 0013 0A06 1013 2013 0014 0A07 1014 2014
    0015 0A08 1015 2015 0016 0A09 1016 2016 0017 0A0A 1017 2017 0018 0A0B 1018 2018
    0019 0B04 1019 2019 001A 0B05 101A 201A 001B 0B06 101B 201B 001C 0B07 101C 201C
    001D 0B08 101D 201D 001E 0B09 101E 201E 001F 0B0A 101F 201F 0020 0B0B 1020 2020
    0021 0C04 1021 2021 0022 0C05 1022 2022 0023 0C06 1023 2023 0024 0C07 1024 2024
    0025 0C08 1025 2025 0026 0C09 1026 2026 0027 0C0A 1027 2027 0028 0C0B 1028 2028
    0029 0D04 1029 2029 002A 0D05 102A 202A 002B 0D06 102B 202B 002C 0D07 102C 202C
    002D 0D08 102D 202D 002E 0D09 102E 202E 002F 0D0A 102F 202F 0030 0D0B 1030 2030
    0031 0E04 1031 2031 0032 0E05 1032 2032 0033 0E06 1033 2033 0034 0E07 1034 2034
    0035 0E08 1035 2035 0036 0E09 1036 2036 0037 0E0A 1037 2037 0038 0E0B 1038 2038
    0039 0F04 1039 2039 003A 0F05 103A 203A 003B 0F06 103B 203B 003C 0F07 103C 203C
    0801 0F08 103D 203D 1001 0F09 103E 203E 1801 0F0A 103F 203F 2001 0F0B 1040 2040
  ".split.map { |x| x.to_i(16) }

  outbuf_bytesize, i = lz_get_outbuf_bytesize(inbuf)
  outbuf = ''

  while i < inbuf.bytesize
    b = inbuf.getbyte(i)
    i += 1
    low = b & 3
    if low > 0
      lv = lookup[b]
      len = lv >> 11
      lz_offset = len.times.inject(lv & 0x700) do |acc, elem|
        acc + (inbuf.getbyte(i + elem) << (elem * 8))
      end
      i += len
      (lv & 0xFF).times { outbuf <<= outbuf.byteslice(-lz_offset, 1) }
    else
      b >>= 2
      if b >= 60
        len = b - 59
        b = len.times.inject(0) do |acc, elem|
          acc + (inbuf.getbyte(i + elem) << (elem * 8))
        end
        i += len
      end
      b += 1
      outbuf <<= inbuf.byteslice(i, b)
      i += b
    end
  end

  return outbuf if outbuf.bytesize == outbuf_bytesize
end

def lz_try_to_decompress_buffer(inbuf)
  lz_decompress_buffer(inbuf)
rescue
  inbuf
end

###############################################################################
# Read the input file and parse it                                            #
###############################################################################

data = JSON.parse(File.binread(ARGV[0]))

data['log']['entries'].each do |k|
  next unless k['request']['method'] == 'POST'

  content = k['response']['content']

  text = content['text']
  text = Base64.decode64(text) if content['encoding'] == 'base64'

  xor_key = find_xor_key(text.byteslice(0, 100), '<?xml version="')
  text = xor_text(text, xor_key) if xor_key

  text = lz_try_to_decompress_buffer(text)

  if convert_to_json && text =~ /^\<\?xml/
    text = JSON.pretty_generate(Hash.from_xml(text))
  end

  puts text
end
