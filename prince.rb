# This is free and unencumbered software released into the public domain.
#
# Anyone is free to copy, modify, publish, use, compile, sell, or
# distribute this software, either in source code form or as a compiled
# binary, for any purpose, commercial or non-commercial, and by any
# means.
#
# In jurisdictions that recognize copyright laws, the author or authors
# of this software dedicate any and all copyright interest in the
# software to the public domain. We make this dedication for the benefit
# of the public at large and to the detriment of our heirs and
# successors. We intend this dedication to be an overt act of
# relinquishment in perpetuity of all present and future rights to this
# software under copyright law.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# For more information, please refer to <http://unlicense.org>

class Prince
  def self.get_max_charisma(prince_level)
    if prince_level <= 100
      27 + prince_level * 3
    elsif prince_level >= 200
      427
    else
      27 + 100 * 3 + (prince_level - 100)
    end
  end

  def self.get_max_stamina(prince_level)
    if prince_level < 101
      12
    elsif prince_level < 120
      13
    elsif prince_level < 140
      14
    elsif prince_level < 160
      15
    elsif prince_level < 180
      16
    elsif prince_level < 200
      17
    else
      18
    end
  end

  # Taken from http://millenniumwaraigis.wikia.com/wiki/Rank#Rank_Exp_Table
  @@exp_tbl = [
    # rubocop:disable all
    [  1,       0], [  2,      16], [  3,      43], [  4,     113],
    [  5,     225], [  6,     470], [  7,     750], [  8,    1100],
    [  9,    1520], [ 10,    2010], [ 11,    2570], [ 12,    3200],
    [ 13,    3900], [ 14,    4670], [ 15,    5510], [ 16,    6420],
    [ 17,    7400], [ 18,    8450], [ 19,    9570], [ 20,   10760],
    [ 21,   12020], [ 22,   13350], [ 23,   14750], [ 24,   16220],
    [ 25,   17760], [ 26,   19370], [ 27,   21050], [ 28,   22800],
    [ 29,   24620], [ 30,   26510], [ 31,   28470], [ 32,   30500],
    [ 33,   32600], [ 34,   34770], [ 35,   37010], [ 36,   39320],
    [ 37,   41700], [ 38,   44150], [ 39,   46670], [ 40,   49260],
    [ 41,   51920], [ 42,   54650], [ 43,   57450], [ 44,   60320],
    [ 45,   63260], [ 46,   66270], [ 47,   69350], [ 48,   72500],
    [ 49,   75720], [ 50,   79010], [ 51,   82370], [ 52,   85800],
    [ 53,   89300], [ 54,   92870], [ 55,   96510], [ 56,  100220],
    [ 57,  104000], [ 58,  107850], [ 59,  111770], [ 60,  115760],
    [ 61,  119820], [ 62,  123950], [ 63,  128150], [ 64,  132420],
    [ 65,  136760], [ 66,  141170], [ 67,  145650], [ 68,  150200],
    [ 69,  154820], [ 70,  159510], [ 71,  164270], [ 72,  169100],
    [ 73,  174000], [ 74,  178970], [ 75,  184010], [ 76,  189120],
    [ 77,  194300], [ 78,  199550], [ 79,  204870], [ 80,  210260],
    [ 81,  215720], [ 82,  221250], [ 83,  226850], [ 84,  232520],
    [ 85,  238260], [ 86,  244070], [ 87,  249950], [ 88,  255900],
    [ 89,  261920], [ 90,  268010], [ 91,  274170], [ 92,  280400],
    [ 93,  286700], [ 94,  293070], [ 95,  299510], [ 96,  306020],
    [ 97,  312600], [ 98,  319250], [ 99,  325970], [100,  332760],
    [101,  339620], [102,  348545], [103,  357596], [104,  366782],
    [105,  376105], [106,  385567], [107,  395170], [108,  404917],
    [109,  414810], [110,  424851], [111,  435042], [112,  445385],
    [113,  455882], [114,  466536], [115,  477350], [116,  488326],
    [117,  499466], [118,  510773], [119,  522249], [120,  533897],
    [121,  545719], [122,  557718], [123,  569895], [124,  582254],
    [125,  594799], [126,  607532], [127,  620454], [128,  633570],
    [129,  646882], [130,  660393], [131,  674106], [132,  688023],
    [133,  702148], [134,  716485], [135,  731037], [136,  745807],
    [137,  760798], [138,  776013], [139,  791456], [140,  807130],
    [141,  823039], [142,  839186], [143,  855575], [144,  872209],
    [145,  889092], [146,  906228], [147,  923621], [148,  941274],
    [149,  959191], [150,  977376], [151,  995833], [152, 1014566],
    [153, 1033579], [154, 1052878], [155, 1072464], [156, 1092344],
    [157, 1112522], [158, 1133002], [159, 1153789], [160, 1174887],
    [161, 1196301], [162, 1218036], [163, 1240097], [164, 1262488],
    [165, 1285214], [166, 1308280], [167, 1331691], [168, 1355453],
    [169, 1379571], [170, 1404050], [171, 1428896], [172, 1454114],
    [173, 1479710], [174, 1505689], [175, 1532057], [176, 1558820],
    [177, 1585984], [178, 1613555], [179, 1641539], [180, 1669942],
    [181, 1698771], [182, 1728032], [183, 1757731], [184, 1787875],
    [185, 1818471], [186, 1849525], [187, 1881044], [188, 1913035],
    [189, 1945505], [190, 1978462], [191, 2011913], [192, 2045865],
    [193, 2080326], [194, 2115303], [195, 2150804], [196, 2186837],
    [197, 2223410], [198, 2260531], [199, 2298208], [200, 2336450],
    [201, 2375265], [202, 2415265], [203, 2455265], [204, 2495265],
    # rubocop:enable all
  ]

  @@stamina_refill_levels = [101, 120, 140, 160, 180, 200]

  def self.lvl_to_exp(prince_level, exp_to_next = nil)
    if prince_level < 1
      0
    elsif prince_level < 201
      return @@exp_tbl[prince_level - 1][1] unless exp_to_next
      @@exp_tbl[prince_level][1] - exp_to_next
    else
      return 2375265 + (prince_level - 201) * 40000 unless exp_to_next
      2375265 + (prince_level - 200) * 40000 - exp_to_next
    end
  end

  def self.exp_to_lvl(total_exp)
    extra_levels = 0
    if total_exp > 2375265
      extra_levels = (total_exp - 2375265) / 40000
      total_exp -= extra_levels * 40000
    end
    tmp = @@exp_tbl.bsearch { |x| x[1] > total_exp }
    prince_level = tmp[0] - 1 + extra_levels
    exp_to_next = tmp[1] - total_exp
    [prince_level, exp_to_next]
  end

  attr_reader :charisma_refill_count, :charisma_refill_val,
              :stamina_refill_count,  :stamina_refill_val,
              :total_exp

  def initialize(prince_level, exp_to_next = nil)
    @total_exp             = Prince.lvl_to_exp(prince_level, exp_to_next)
    @charisma_refill_count = 0
    @charisma_refill_val   = 0
    @stamina_refill_count  = 0
    @stamina_refill_val    = 0
  end

  def gain_exp(exp_bonus)
    return if exp_bonus <= 0
    prince_level, exp_to_next = Prince.exp_to_lvl(@total_exp)
    while exp_to_next <= exp_bonus
      @total_exp += exp_to_next
      exp_bonus -= exp_to_next
      prince_level, exp_to_next = Prince.exp_to_lvl(@total_exp)
      @charisma_refill_count += 1
      @charisma_refill_val   += Prince.get_max_charisma(prince_level)
      if @@stamina_refill_levels.include?(prince_level) || prince_level > 200
        @stamina_refill_count += 1
        @stamina_refill_val   += Prince.get_max_stamina(prince_level)
      end
    end
    @total_exp += exp_bonus
  end
end
